# RobotArt Docker 2018

This is a container for all RobotArt files. Each part is an individual git submodule.

## Setup

Install Docker.

If your computer has Nvidia drivers install nvidia-docker version 1.

Clone this repository. Inside this repository, run `git submodule update --init --recursive`.

## Run 

`$ ./run.sh` or `./run.sh sim` for the Redhawk simulation.

To attach to the container, run: 

`$ docker exec -i -t roboteam /bin/bash`

When connecting to the RedHawk arm, make sure your IP address is configured statically to 172.21.0.123, with netmask 255.255.0.0 and gateway 172.21.0.1.